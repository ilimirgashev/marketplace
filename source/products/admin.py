from django.contrib import admin
from products.models import Product, ProductItemPictures, ProductItem, ProductCategory, Variation, VariationOption, OrderStatus

admin.site.register(Product)
admin.site.register(ProductItem)
admin.site.register(ProductItemPictures)
admin.site.register(ProductCategory)
admin.site.register(Variation)
admin.site.register(VariationOption)
admin.site.register(OrderStatus)

