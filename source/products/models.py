from django.db import models


class ProductCategory(models.Model):
    parent_category = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL)
    category_name = models.CharField(max_length=255)

    def __str__(self):
        return f'Category: {self.category_name}'

class Variation(models.Model):
    category = models.ForeignKey('products.ProductCategory', related_name='variation', on_delete=models.PROTECT)
    name = models.CharField(max_length=255, null=False, blank=False)

    def __str__(self):
        return f'{self.name}'

class VariationOption(models.Model):
    variation = models.ForeignKey('products.Variation', related_name='option',on_delete=models.PROTECT)
    value = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.variation}: {self.value}'

class Product(models.Model):
    author = models.ForeignKey('accounts.PartnerUser', on_delete=models.PROTECT)
    category = models.ForeignKey('products.ProductCategory', related_name='products', on_delete=models.PROTECT)
    name = models.CharField(max_length=255, null=False, blank=False, verbose_name='Название')
    description = models.TextField(max_length=1500,  null=False, blank=False, verbose_name='Описание')
    product_image = models.ImageField(upload_to='product_picture/')
    brand = models.CharField(max_length=40, verbose_name='Бренд')
    create = models.DateTimeField(auto_now_add=True, verbose_name='Создание')
    update = models.DateTimeField(auto_now=True, verbose_name='Обновление')

    def __str__(self):
        return f'{self.name} {self.description} {self.brand}'


class ProductItem(models.Model):
    product = models.ForeignKey('products.Product', related_name='items', on_delete=models.PROTECT)
    SKU = models.CharField(max_length=50, unique=True, verbose_name='Артикул')
    qty_in_stock = models.PositiveIntegerField(null=False, blank=False, verbose_name='Количество')
    price = models.DecimalField(max_digits=10, decimal_places=2, null=False, blank=False, verbose_name='Цена')
    options = models.ManyToManyField('products.VariationOption', related_name='products')

    def __str__(self):
        return f'{self.product} {self.qty_in_stock}, {self.price}, {self.options.all()}'


class ProductItemPictures(models.Model):
    product_item = models.ForeignKey('products.ProductItem', related_name='pictures', on_delete=models.PROTECT)
    product_image = models.ImageField(upload_to='product_items_pictures/')



class ShoppingCart(models.Model):
    user = models.OneToOneField('accounts.Customer', on_delete=models.PROTECT)
    products = models.ManyToManyField('products.ProductItem', related_name='cart', through='products.ShoppingCartItem')

    def __str__(self):
        return f'Cart: {self.user} X {self.products}'


class ShoppingCartItem(models.Model):
    cart = models.ForeignKey('products.ShoppingCart', on_delete=models.PROTECT)
    product_item = models.ForeignKey('products.ProductItem', on_delete=models.PROTECT)
    qty = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f'{self.product_item} {self.qty}'

    def get_total(self):
        return self.qty * self.product_item.price

class OrderStatus(models.Model):
    STATUS_CHOICES = [
        ('pending', 'В Ожидании'),
        ('processed', 'Обработанный'),
        ('shipped', 'Отправленный'),
        ('delivered',  'Доставлен')
    ]
    status = models.CharField(max_length=255, choices=STATUS_CHOICES)


class OrderProduct(models.Model):
    product_item = models.ForeignKey('products.ProductItem', on_delete=models.PROTECT)
    order = models.ForeignKey('products.ShopOrder', on_delete=models.PROTECT)
    qty = models.PositiveIntegerField(default=1)
    price = models.DecimalField(max_digits=10, decimal_places=2)


class ShopOrder(models.Model):
    user = models.ForeignKey('accounts.Customer', on_delete=models.PROTECT)
    products = models.ManyToManyField('products.ProductItem', related_name='order', through='products.OrderProduct')
    order_date = models.DateTimeField(auto_now_add=True)
    payment_method = models.ForeignKey('accounts.UserPaymentMethod', on_delete=models.SET_NULL, null=True)
    shipping_address = models.ForeignKey('accounts.Address', on_delete=models.SET_NULL, null=True)
    order_total = models.DecimalField(max_digits=10, decimal_places=2)
    order_status = models.ForeignKey('products.OrderStatus', on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)

