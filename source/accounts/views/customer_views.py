from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.contrib.auth import authenticate, login
from accounts.serializers.customer_serializers import CustomUserSerializer
from rest_framework.permissions import AllowAny


@api_view(['POST'])
@permission_classes([AllowAny])
def signup_view(request):
    serializer = CustomUserSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    else:
        return Response(serializer.data, status=400)


@api_view(['POST'])
def login_veiw(request):
    email = request.data.get('email')
    password = request.data.get('password')

    user = authenticate(request, email=email, password=password)

    if user is not None:
        if user.is_active:
            login(request, user)
            return Response({"reposne":"you logged in successfully"}, status=200)
        else:
            return Response({"reposne":"Your account is not active"}, status=400)
    else:
        return Response({"reposne":"Incorrect data'"}, status=401)


