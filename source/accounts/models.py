from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class CustomUserManager(BaseUserManager):
    def create_user(self, email, phone_number, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            phone_number=phone_number,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, phone_number, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(email,
                                password=password,
                                phone_number=phone_number,
                                )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True, verbose_name='email')
    phone_number = models.CharField(unique=True, verbose_name='Номер телефона', null=True, blank=True, max_length=16)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number']

    def __str__(self):
        return self.email


class Customer(models.Model):
    user = models.OneToOneField('accounts.CustomUser', related_name='customer',on_delete=models.PROTECT, primary_key=True)
    name = models.CharField(max_length=255, null=False, blank=False, verbose_name='Имя')
    surname = models.CharField(max_length=255, null=False, blank=False, verbose_name='Фамилия')


class PartnerUser(models.Model):
    user = models.OneToOneField(CustomUser, related_name='partner', on_delete=models.PROTECT, primary_key=True)
    username = models.CharField(max_length=255 , null=False, blank=False, verbose_name='Никнейм')
    name = models.CharField(max_length=255, null=False, blank=False, verbose_name='Имя')
    surname = models.CharField(max_length=255, null=False, blank=False, verbose_name='Фамилия')
    brand = models.CharField(max_length=255, null=False, blank=False, verbose_name='Бренд')
    certificate = models.BooleanField(default=False)
    img = models.ImageField(upload_to='partner_photo', verbose_name='Аватар')
    description = models.TextField(max_length=250, null=False, blank=False, verbose_name='Описания')
    black_list = models.BooleanField(default=False)


class PartnerAddress(models.Model):
    partner = models.ForeignKey('accounts.PartnerUser', related_name="addresses", on_delete=models.PROTECT)
    unit_number = models.CharField(max_length=10)
    street_number = models.CharField(max_length=10)
    address_line1 = models.CharField(max_length=255)
    address_line2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    postal_code = models.CharField(max_length=10)
    is_default = models.BooleanField(default=False)


class PartnersPaymentType(models.Model):
    values = models.CharField(max_length=255)


class PartnersPaymentMethod(models.Model):
    partner = models.ForeignKey(PartnerUser, on_delete=models.PROTECT)
    payment_type = models.ForeignKey('accounts.PartnersPaymentType', on_delete=models.PROTECT)
    provider = models.CharField(max_length=255)
    account_number = models.CharField(max_length=255)
    expiry_date = models.DateField()
    is_default = models.BooleanField(default=False)


class WorkshopUser(models.Model):
    user = models.OneToOneField('accounts.CustomUser', related_name='workshop',on_delete=models.PROTECT, primary_key=True)
    name = models.CharField(max_length=255 , null=False, blank=False, verbose_name='Название Цеха')
    work_since = models.DateField(null=False, blank=False, verbose_name='Стаж')
    employees_quantity = models.PositiveIntegerField()


class Country(models.Model):
    country_name = models.CharField(max_length=255)


class Address(models.Model):
    user = models.ForeignKey('accounts.Customer', related_name="addresses", on_delete=models.PROTECT)
    unit_number = models.CharField(max_length=10)
    street_number = models.CharField(max_length=10)
    address_line1 = models.CharField(max_length=255)
    address_line2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    postal_code = models.CharField(max_length=10)
    country = models.ForeignKey('accounts.Country', on_delete=models.PROTECT)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.address_line1}, {self.city}, {self.country}'

    @classmethod
    def get_default_address(cls, user):
        try:
            return cls.objects.get(user=user, is_default=True)
        except cls.DoesNotExist:
            return None

class PaymentType(models.Model):
    value = models.CharField(max_length=255)


class UserPaymentMethod(models.Model):
    user = models.ForeignKey(CustomUser, related_name='payment_method', on_delete=models.PROTECT)
    payment_type = models.ForeignKey('accounts.PaymentType', related_name='payment_type', on_delete=models.PROTECT)
    provider = models.CharField(max_length=255)
    account_number = models.CharField(max_length=255)
    expiry_date = models.DateField()
    is_default = models.BooleanField(default=False)


class UserReview(models.Model):
    user = models.OneToOneField('accounts.Customer', on_delete=models.PROTECT)
    rating_value = models.PositiveIntegerField()
    comment = models.TextField(blank=True)
    ordered_prodcut_id = models.ForeignKey('products.ShopOrder', on_delete=models.PROTECT)


