# Generated by Django 4.2.7 on 2023-11-24 20:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='name',
            field=models.CharField(default='ara', max_length=255, verbose_name='Имя'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customer',
            name='surname',
            field=models.CharField(default='kuliev', max_length=255, verbose_name='Фамилия'),
            preserve_default=False,
        ),
    ]
