from django.contrib import admin
from .models import (CustomUser,
                     Customer, PartnerUser,
                     PartnerAddress, PaymentType,
                     PartnersPaymentType, WorkshopUser,
                     UserReview, Address,
                     UserPaymentMethod)


admin.site.register(CustomUser)
admin.site.register(Customer)
admin.site.register(PartnerUser)
admin.site.register(PartnerAddress)
admin.site.register(PaymentType)
admin.site.register(PartnersPaymentType)
admin.site.register(WorkshopUser)
admin.site.register(UserReview)
admin.site.register(Address)
admin.site.register(UserPaymentMethod)