from django.urls import path
from .views.customer_views import login_veiw, signup_view

app_name = 'accounts'

urlpatterns = [
    path('login/', login_veiw, name='login'),
    path('signup/', signup_view, name='signup'),

]
