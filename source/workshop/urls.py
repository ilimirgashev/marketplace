from django.urls import path, include
from workshop.views import WorkShopProductViewSet, WorkShopProductCategoryViewSet, WorkShopProductPicturesViewSet
from rest_framework.routers import DefaultRouter

app_name = "workshop_urls_api"

router = DefaultRouter()
router.register('workshopproduct', WorkShopProductViewSet, basename='WorkShopProduct')
router.register('workshopproductcategory', WorkShopProductCategoryViewSet, basename='WorkShopProductCategory')
router.register('workshopproductpictures', WorkShopProductPicturesViewSet, basename='WorkShopProductPictures')

urlpatterns = [
    path('', include(router.urls)),
]