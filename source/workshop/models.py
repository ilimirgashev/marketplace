from django.db import models


class WorkShopProduct(models.Model):
    author = models.ForeignKey('accounts.WorkshopUser', on_delete=models.PROTECT)
    name = models.CharField(max_length=255, null=False, blank=False, verbose_name='Название')
    description = models.TextField(max_length=1500,  null=False, blank=False, verbose_name='Описание')
    product_image = models.ImageField(upload_to='product_picture/')
    brand = models.CharField(max_length=40, verbose_name='Бренд')
    create = models.DateTimeField(auto_now_add=True, verbose_name='Создание')
    update = models.DateTimeField(auto_now=True, verbose_name='Обновление')

    def __str__(self):
        return f'{self.name} {self.description} {self.brand}'

class WorkShopProductCategory(models.Model):
    product = models.ForeignKey('workshop.WorkShopProduct', related_name='category', on_delete=models.PROTECT)
    category_name = models.CharField(max_length=255)

    def __str__(self):
        return f'Category: {self.category_name}'


class WorkShopProductPictures(models.Model):
    product = models.ForeignKey('workshop.WorkShopProduct', related_name='pictures', on_delete=models.PROTECT)
    product_image = models.ImageField(upload_to='workshop_product_pictures/')

    def __str__(self):
        return f'Image for {self.product.name} ({self.product.brand})'
