from workshop.serializers import WorkShopProductSerializer, WorkShopProductCategorySerializer, WorkShopProductPicturesSerializer
from workshop.models import WorkShopProduct, WorkShopProductCategory, WorkShopProductPictures
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

# -----------------------

# Круды для моделей в разделе WorkShopProduct

# -----------------------

# Круд для WorkShopProduct

class WorkShopProductViewSet(viewsets.ViewSet):
    def list(self, request):
        # get /workshop/workshopproduct/ get запрос
        products = WorkShopProduct.objects.all()
        serializer = WorkShopProductSerializer(products, many=True)
        return Response(serializer.data)

    def create(self, request):
        # create /workshop/workshopproduct/ post запрос
        serializer = WorkShopProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        # delete /workshop/workshopproduct/айди объекта/
        try:
            product = WorkShopProduct.objects.get(pk=pk)
        except WorkShopProduct.DoesNotExist:
            return Response({'detail': 'Product not found 🤔'}, status=status.HTTP_404_NOT_FOUND)
        product.delete()
        return Response({'detail': 'Product deleted successfully 😎'}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request, pk=None):
        # update /workshop/workshopproduct/айди объекта/
        try:
            product = WorkShopProduct.objects.get(pk=pk)
        except WorkShopProduct.DoesNotExist:
            return Response({'detail': 'Product not found 😖'}, status=status.HTTP_404_NOT_FOUND)
        serializer = WorkShopProductSerializer(product, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# -----------------------

# Круд для WorkShopProductCategory

class WorkShopProductCategoryViewSet(viewsets.ViewSet):
    def list(self, request):
        # get /workshop/workshopproductcategory/ get запрос
        category = WorkShopProductCategory.objects.all()
        serializer = WorkShopProductCategorySerializer(category, many=True)
        return Response(serializer.data)

    def create(self, request):
        # create /workshop/workshopproductcategory/ post запрос
        serializer = WorkShopProductCategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        # delete /workshop/workshopproductcategory/айди объекта/
        try:
            category = WorkShopProductCategory.objects.get(pk=pk)
        except WorkShopProductCategory.DoesNotExist:
            return Response({'detail': 'Product category not found 😶'}, status=status.HTTP_404_NOT_FOUND)
        category.delete()
        return Response({'detail': 'Product category deleted successfully 🤠'}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request, pk=None):
        # update /workshop/workshopproductcategory/айди объекта/
        try:
            category = WorkShopProductCategory.objects.get(pk=pk)
        except WorkShopProductCategory.DoesNotExist:
            return Response({'detail': 'Product category not found 🤕'}, status=status.HTTP_404_NOT_FOUND)
        serializer = WorkShopProductCategorySerializer(category, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# -----------------------

# Круд для WorkShopProductPictures

class WorkShopProductPicturesViewSet(viewsets.ViewSet):
    def list(self, request):
        # get /workshop/workshopproductpictures/ get запрос
        pictures = WorkShopProductPictures.objects.all()
        serializer = WorkShopProductPicturesSerializer(pictures, many=True)
        return Response(serializer.data)

    def create(self, request):
        # create /workshop/workshopproductpictures/ post запрос
        serializer = WorkShopProductPicturesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        # delete /workshop/workshopproductpictures/айди объекта/
        try:
            pictures = WorkShopProductPictures.objects.get(pk=pk)
        except WorkShopProductPictures.DoesNotExist:
            return Response({'detail': 'Product pictures not found 😓'}, status=status.HTTP_404_NOT_FOUND)
        pictures.delete()
        return Response({'detail': 'Product pictures deleted successfully 🙃'}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request, pk=None):
        # update /workshop/workshopproductpictures/айди объекта/
        try:
            pictures = WorkShopProductPictures.objects.get(pk=pk)
        except WorkShopProductPictures.DoesNotExist:
            return Response({'detail': 'Product pictures not found 😵‍💫'}, status=status.HTTP_404_NOT_FOUND)
        serializer = WorkShopProductPicturesSerializer(pictures, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# -----------------------
