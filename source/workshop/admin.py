from django.contrib import admin
from workshop.models import WorkShopProduct, WorkShopProductCategory, WorkShopProductPictures

admin.site.register(WorkShopProduct)
admin.site.register(WorkShopProductCategory)
admin.site.register(WorkShopProductPictures)