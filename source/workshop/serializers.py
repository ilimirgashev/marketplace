from workshop.models import WorkShopProduct, WorkShopProductCategory, WorkShopProductPictures
from rest_framework import serializers

class WorkShopProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkShopProduct
        fields = ('id', 'author', 'name', 'description', 'product_image', 'brand', 'create', 'update')
        read_only_fields = ['id']

class WorkShopProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkShopProductCategory
        fields = ('id', 'product', 'category_name')
        read_only_fields = ['id']

class WorkShopProductPicturesSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkShopProductPictures
        fields = ('id', 'product', 'product_image')
        read_only_fields = ['id']


